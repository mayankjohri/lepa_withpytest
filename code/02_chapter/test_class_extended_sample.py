from app import add, multiply

class TestMathsFunctions(object):

    def setup(self):
        print("inside setup multiply")

    def logging(self, msg):
        print(msg, "\n")

    def test_add(self):
        assert add(2, 3) == 5
        self.logging("test_add passed")

    def test_add_string(self):
        assert add("Ich", " bin") == "Ich bin"

    def test_multiple(self):
        assert multiply(10, 2) == 20
        assert multiply("India ", 2) == "India India "
