"""Basic Example of Pytest"""
import pytest

from app import add

def setup():
    print("Inside test setup")

def teardown():
    print("Inside test teardown")

def setup_module():
    print("Inside test setup_module")

def teardown_module():
    print("Inside test teardown_module")


def test_sum_ints():
    """
    Tests `add` function against two ints.
    """
    assert add(10, 5) == 15

@pytest.mark.staging
def test_sum_strings():
    """
    Tests `add` function against two ints.
    """
    assert add("Ich", " bin") == "Ich bin"
