"""Basic Example of Pytest"""

from time import sleep
def add(first, second):
    """
    Returns sum of `first` & `second`
    """
    print("Lets go to sleep for 2 seconds")
    sleep(2)
    return first + second


def setup_module():
    sleep(1)
    print("Inside setup_module")

def test_slow_running_sum_ints():
    """
    Tests `add` function against two ints.
    """
    assert add(10, 5) == 15
