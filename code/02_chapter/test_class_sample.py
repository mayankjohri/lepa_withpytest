from app import add

class TestMathsFunctions(object):

    def test_add(self):
        assert add(2, 3) == 5

    def test_add_string(self):
        assert add("Ich", " bin") == "Ich bin"
