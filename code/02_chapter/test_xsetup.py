"""Basic Example of Pytest"""
import pytest

from app import add

class TestxSetup(object):
    def setup(self):
        print("Inside test setup")

    def teardown(self):
        print("Inside test teardown")

    @classmethod
    def setup_class(cls):
        print("Inside test setup_module")

    @classmethod
    def teardown_class(cls):
        print("Inside test teardown_module")


    def test_sum_ints(self):
        """
        Tests `add` function against two ints.
        """
        assert add(10, 5) == 15

    @pytest.mark.staging
    def test_sum_strings(self):
        """
        Tests `add` function against two ints.
        """
        assert add("Ich", " bin") == "Ich bin"
