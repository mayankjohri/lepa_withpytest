"""Basic Example of Pytest"""
import pytest

from app import add

def test_sum_ints():
    """
    Tests `add` function against two ints.
    """
    assert add(10, 5) == 15

@pytest.mark.staging
def test_sum_strings():
    """
    Tests `add` function against two ints.
    """
    assert add("Ich", " bin") == "Ich bin"
