"""Basic Example of Pytest"""


def add(first, second):
    """
    Returns sum of `first` & `second`
    """
    return first + second


def test_sum_ints():
    """
    Tests `add` function against two ints.
    """
    assert add(10, 5) == 15
