
import pytest
from app import square


@pytest.mark.parametrize("val, expected_result", [pytest.param(1, 1, id="Multiple by One"), 
                        (2, 4), (3, 9)])
def test_square(val, expected_result):
    actual = square(val)
    assert expected_result == actual
