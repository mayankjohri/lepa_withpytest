import pytest

from app import square


@pytest.mark.parametrize("os_name", ("Windows", "ReactOS", "Linux"))
@pytest.mark.parametrize("val, expected", ((1, 1), (2, 4), (3, 9)))
@pytest.mark.slow
def test_square(os_name, val, expected):
    print(f"Testing on {os_name}")
    actual = square(val)
    assert expected == actual
