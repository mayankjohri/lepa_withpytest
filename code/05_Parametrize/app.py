
def add(first, second):
    """
    Returns sum of `first` & `second`

    """

    return first + second

def multiply(first, second):
    """
    Returns the multiplication of `first` & `second`
    """

    return first * second

def square(val):
    return val * val
