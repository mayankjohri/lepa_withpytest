

def pytest_generate_tests(metafunc):
    print(metafunc.fixturenames)
    if "windows" in metafunc.fixturenames:
        metafunc.parametrize("windows", ["one", "uno"])
    if "linux" in metafunc.fixturenames:
        metafunc.parametrize("linux", ["two", "duo"])


def test_os_vals(windows, linux):
    print(windows, ":",  linux)
