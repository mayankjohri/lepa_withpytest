import pytest

from app import square


@pytest.mark.parametrize("name, val, expected_result", [("One", 1, 1), ("square of Two", 2, 4), ("Square of 3", 3, 9)])
def test_square(name, val, expected_result):
    actual = square(val)
    assert expected_result == actual
