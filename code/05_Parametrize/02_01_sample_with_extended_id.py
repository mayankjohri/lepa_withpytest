import pytest 
from app import square

test_data = [
    (1, 1), (2, 4), (3,9 )
]

# ids
ids = ["square({}) = expect:{}".format(a, expect) for a, expect in test_data]

@pytest.mark.parametrize("val, expected_result", test_data, ids=ids)
def test_square(val, expected_result):
    actual = square(val)
    assert expected_result == actual
