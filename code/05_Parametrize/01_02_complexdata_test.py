import pytest

from app import square

test_data = (
    ("nodes", {
        1: "all",
        2: "node"
    }, ("fail", "fail")),

    ("nodes", {
        1: "all",
        2: "node"
    }, ("pass", "fail"))
)


@pytest.mark.parametrize("req_type, nodes_seq, expected_resutls", test_data)
def test_square(req_type, nodes_seq, expected_resutls):
    print(req_type, nodes_seq, expected_resutls)
