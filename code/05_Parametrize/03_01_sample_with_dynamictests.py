import pytest

from app import square

def pytest_generate_tests(metafunc):
    print(dir(metafunc))
    metafunc.parametrize("val", (1, 2 ,3))

def test_sample(val):
    print(val)

# @pytest.mark.parametrize("os_name", ("Windows", "ReactOS", "Linux"))
# @pytest.mark.parametrize("val, expected", ((1, 1), (2, 4), (3, 9)))
# def test_square(os_name, val, expected):
#    print(f"Testing on {os_name}")
#    actual = square(val)
#    assert expected == actual
