import pytest

from app import square


@pytest.mark.parametrize("val, expected_result", [(1, 1), (2, 4), (3, 9)])
@pytest.mark.env("smoke")
def test_square(val, expected_result):
    actual = square(val)
    assert expected_result == actual
