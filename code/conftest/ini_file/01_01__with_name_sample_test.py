import pytest
import sys

from app import square

@pytest.mark.skip()
def test_square_float():
    val, expected_result = 2.2, 4.4
    actual = square(val)
    assert expected_result == actual

@pytest.mark.skip(reason="Code under development")
def test_square():
    val, expected_result = 2, 4
    actual = square(val)
    assert expected_result == actual


@pytest.mark.skipif(sys.platform != "win32", reason="Only runs on ReactOS")
def test_square_odd_num():
    val, expected_result = 3, 9
    actual = square(val)
    assert expected_result == actual

@pytest.mark.parametrize(
    ("n", "expected"),
    [
        pytest.param(1, 0, marks=pytest.mark.xfail),
        pytest.param(1, 3, marks=pytest.mark.xfail(reason="some bug")),
        (2, 4),
        pytest.param(
            10, 100, marks=pytest.mark.skipif(sys.version_info >= (3, 0), reason="py2k")
        ),
	(11, 121),
        (12, 142)
    ],
)
def test_multiple_vals(n, expected):
    if n == 11:
        print(f"n == {n}")
        pytest.xfail("Programmatically failing the testcase")
    assert n != 12, "For training failing the testcase"
    assert square(n) == expected


def test_os():
    if sys.version[0] >= "3":
        pytest.skip(reason="py2k")

# @pytest.mark.skipif(pytest.importorskip("reactos"), reason="module not found")
# def test_square_one():
#     val, expected_result = 1, 1
#     actual = square(val)
#     assert expected_result == actual
