import pytest


# def pytest_addoption(parser):
#     parser.addoption(
#         "-E",
#         action="store",
#         metavar="NAME",
#         help="only run tests matching the environment NAME.",
#     )


# def pytest_configure(config):
#     # register an additional marker
#     config.addinivalue_line(
#         "markers", "env(name): mark test to run only on named environment"
#     )


# def pytest_runtest_setup(item):
#     print("Inside pytestsetupenv")
#     envnames = [mark.args[0] for mark in item.iter_markers(name="env")]
#     print(f"envnames: {envnames}")
#     if envnames:
#         if item.config.getoption("-E") not in envnames:
#             pytest.skip("test requires env in {!r}".format(envnames))
#     else:
#         pytest.skip("Please provide an env in your testcases")
