from allure import step


@step
def add(first, second):
    """
    Returns sum of `first` & `second`

    """

    return first + second

@step
def multiply(first, second):
    """
    Returns the multiplication of `first` & `second`
    """

    return first * second

@step
def square(val):
    return val * val
