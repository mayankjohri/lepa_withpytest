import logging

import pytest


from app import square


LOGGER = logging.getLogger(__name__)

@pytest.mark.parametrize("val, expected_result", [(1, 1), (2, 4), (3, 9)])
@pytest.mark.env("smoke")
def test_square(val, expected_result):
    LOGGER.info(f"Lets start the testing of square")
    LOGGER.debug(f"with data {val} and expected value: {expected_result}")
    actual = square(val)
    assert expected_result == actual
