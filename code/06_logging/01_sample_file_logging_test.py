import logging

import pytest
import allure 

from app import square



def test_logging():
    for i in range(5):
        print(f'This is print statement: {i}')
        allure.attach('Value of i', f'Value of i {i}')
        logging.critical(f'This is critical error: {i}')
        logging.debug(f'This is debugging: {i}')
    assert True


@pytest.mark.parametrize("val, expected_result", [(1, 1), (2, 5), (3, 9)])
@pytest.mark.env("smoke")
def test_square(val, expected_result):
    logging.info(f"Lets start the testing of square")
    logging.debug(f"with data {val} and expected value: {expected_result}")
    actual = square(val)
    assert expected_result == actual, "Issue with assert"
