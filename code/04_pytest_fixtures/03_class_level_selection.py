import pytest

@pytest.fixture(scope="class")
def my_fixture():
    print ("This is a setup section of fixture")
    yield
    print("This is teardown section of fixture")


@pytest.mark.usefixtures("my_fixture")
class TestIt:
    def test_tc1(self):
        assert 1
    
    def test_tc2(self):
        print("Test tc2 testcase")

    def test_tc3(self):
        print("test_tc3 testcase")
