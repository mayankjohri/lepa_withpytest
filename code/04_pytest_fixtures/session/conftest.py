import pytest


@pytest.fixture(scope="session", autouse=True)
def my_fixture(request):
    print ("This is a fixture")
    print(f"{request=}")
    session = request.node
    print(session)
    for item in session.items:
        print(item)
    yield
    print("Post execution...")
    session = request.node
    print(session)
