
def add(first, second):
    """
    Returns sum of `first` & `second`

    """
    print(f"Inside Add function: {first}, {second}")
    return first + second

def multiply(first, second):
    """
    Returns the multiplication of `first` & `second`
    """
    print(f"Inside multiply function: {first}, {second}")
    return first * second

