import pytest


@pytest.fixture()
def my_fixture():
    print ("This is a fixture")
    yield
    print("This is teardown")

def test_my_fixture(my_fixture):
    print ("I'm the test")
