"""Basic Example of Pytest"""
import pytest

from app import add

@pytest.fixture(scope="module")
def my_fix():
    print("\nSetup of fix")
    yield
    print("\nTeardown of fixture")


def test_sum_list():
    """
    """
    print("Lets start the list sum test")

def test_sum_ints(my_fix):
    """
    Tests `add` function against two ints.
    """
    print("Lets stat the test")
    assert add(10, 5) == 15

def test_sum_list_update(my_fix):
    print("inside test for list")

def test_sum_strs(my_fix):
    """
    Tests `add` function against two strs.
    """
    print("Lets stat the test")
    assert add("Ja", " ich") == "Ja ich"
