"""Basic Example of Pytest"""
import pytest

@pytest.fixture(scope="module", params=["Windows", "Linux"])
def my_fix(request):
    # global os_name
    os_name = request.param
    print(f"\nSetup of fix: {request.param}")
    yield os_name
    print("\nTeardown of fixture")


def add(first, second):
    """
    Returns sum of `first` & `second`
    """
    return first + second


def test_sum_ints(my_fix):
    """
    Tests `add` function against two ints.
    """
    os_name = my_fix
    print(f"Lets stat the test for os: {os_name}")
    assert add(10, 5) == 15

def test_sum_strs(my_fix):
    """
    Tests `add` function against two strs.
    """
    os_name = my_fix
    print(f"Lets stat the test for os: {os_name}")
    assert add("Ja", " ich") == "Ja ich"
