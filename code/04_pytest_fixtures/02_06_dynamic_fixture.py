# 02_01_function_level_selection.py

import pytest

def find_scope(fixture_name, config):
    return config.getoption("--scope", "function")


@pytest.fixture(scope=find_scope)
def dyn_fixture():
    print("Inside my_fixture")


@pytest.fixture(scope="function")
def tester():
    """Create tester object"""
    print("Inside tester fixture")

@pytest.fixture()
def my_fixture():
    print ("This is a fixture")


def test_my_fixture(my_fixture):
    print ("I'm the test")

def test_without_fixture():
    print("Inside test without fixture")


class TestIt:
    def test_tc1(self, tester):
       print("Inside test")
       assert 1

    def test_tc2(self,dyn_fixture):
        print("Inside test_tc2")
    
    def test_tc3(self):
        print("Inside test_tc3")

