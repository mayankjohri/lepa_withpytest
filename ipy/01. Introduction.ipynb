{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Getting used to writing testing code and running this code in parallel is now considered a good habit. Used wisely, this method helps you define more precisely your code's intent and have a more decoupled architecture."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basics of Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic suggestions for automating a testcase"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets discuss few suggestions, while automating the test cases, if followed might help you better test your application."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Being Independent is Great"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The individual test cases \n",
    "- must test **only one small** independent functionality of the application.\n",
    "- must be able to run **alone** or as part of entire test suite \n",
    "- must be able to preferably run irrespective of the order in which they are called. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning is Good"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The individual test cases \n",
    "- should be able to leave system cleanly.\n",
    "- should not leave any residue on the system in all cases except in cases where its required."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Fast Runner wins the race"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The test cases\n",
    "- should run as fast as possible without effecting the result.\n",
    "- if slow, should be marked as such so they can be executed only when needed.\n",
    "- should be able to run in parallel on multiple systems (if possible) decreasing the overall execution time of the suite"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Basic Suggestions for testers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Know about your trade and always try to learn new tricks.\n",
    "- Run all the tests daily in the form of Nightly runs.\n",
    "- Learn how to run a single test or a test suite or tests from same or different modules.\n",
    "- Always use descriptive name for the test case so that they can convey as much as information as possible\n",
    "- Use good reporting framework.\n",
    "- Reporting should be as exhaustive as possible. \n",
    "- Provide as much logging as possible and be descriptive in your logging. \n",
    "- When reporting a production/Manual QA reported bug, its a good idea to create a new test case related to it, so that once developers fixes the bug it can not only be validated but also it will act as a check against future occurrence."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Basic Suggestions for Developers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Run the full test suite at minimum \n",
    "    - before the start of development session (sprint),\n",
    "      so that we know what is working and what is already broken. \n",
    "    - middle of development session (sprint)\n",
    "    - just before the end of development session (sprint)\n",
    "- It would be ideal that full test suite is executed every night to find the health of the application. \n",
    "- Select areas of importance and let the test cases related to them pass successfully before any changes be allowed to merge in the master branch."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python ecosystem provides multiple frameworks to write the test cases such as \n",
    "- `unittest`\n",
    "- `doctest`\n",
    "- `robot`\n",
    "- `PyTest`\n",
    "- `Nose2`\n",
    "- `Testify`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `pytest`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `pytest` framework are one of the most advanced & flexible testing frameworks available in `Python`. They can be used for your smallest to largest testing needs. \n",
    "\n",
    "It also supports third party plugins and there are litterally hundreds to them. The consolidated list of plugins can be found at https://pytest.readthedocs.io/en/2.7.3/plugins_index/index.html. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**An example of a simple test:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from app import increment\n",
    "\n",
    "def test_will_fail():\n",
    "    \"\"\"\n",
    "        \"increment(3)\" should return 4, but since we are\n",
    "        evaluating it against 5, the test should fail.\n",
    "    \"\"\"\n",
    "    assert increment(3) == 5\n",
    "\n",
    "\n",
    "def test_will_pass():\n",
    "    \"\"\".\"\"\"\n",
    "    assert increment(4) == 5\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Benefits "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Allows for compact test suites\n",
    "> The idioms that `pytest` first introduced brought a change in the Python community because they made it possible for test suites to be written in a very compact style, or at least far more compact than was ever possible before.\n",
    "> Pytest basically introduced the concept that Python tests should be plain Python functions instead of forcing developers to include their tests inside large test classes.\n",
    "\n",
    "- Minimal boilerplate\n",
    "> Tests written with `pytest` need very little boilerplate code, which makes them easy to write and understand.\n",
    "\n",
    "- Tests parametrization\n",
    "> You can parametrize any test and cover all uses of a unit without code duplication.\n",
    "\n",
    "- Very pretty and useful failure information\n",
    "> `Pytest` rewrites your test so that it can store all intermediate values that can lead to failing `assert` and provides you with very pretty explanation about what has been asserted and what have failed.\n",
    "\n",
    "- `fixture`'s are simple and easy to use\n",
    "> A `fixture` is just a function that returns a value and to use a fixture you just have to add an argument to your test function. You can also use a fixture from another fixture in the same manner, so it's easy to make them modular.\n",
    "You can also parametrize fixture and every test that uses it will run with all values of parameters, no test rewrite needed. If your test uses several fixtures, all parameters' combinations will be covered.\n",
    "\n",
    "- Pdb just works\n",
    "> Pytest `**automagically**` (and safely) disables output capturing when you're entering pdb, so you don't have to redirect debugger to other console or bear huge amount of unneeded output from other tests.\n",
    "\n",
    "- Test discovery by file-path\n",
    "\n",
    "- Over 150 plugins \n",
    "> more than 150 plugins to customise py.test such as pytest-BDD and pytest-konira for writing tests for Behaviour Driven Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Issues "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Compatibility Issues\n",
    "> The fact that pytest uses it's own special routines to write tests means that you are trading convenience for compatibility. In other words, writing tests for pytest means that you are tying yourself to only pytest and the only way to use another testing framework is to rewrite most of the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Salient Features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Pytest Fixtures\n",
    "- Introspect agent\n",
    "- Test parametrization\n",
    "- Pytest Markers (Custom and inbuilt)\n",
    "- Thousands of Plugins"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installing `pytest`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`pytest` is not part of default installation of python and needs to be manually installed, which can be done by executing following command "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Per user account"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "pip install -U pytest --user\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### In virtualenv (recommended)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please refer to section `apendix -> virtualenv` for more details on installing and using virtualenv.\n",
    "\n",
    "Run the following command\n",
    "```bash\n",
    "pip install -U pytest\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Install Globally (not recommended)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following command\n",
    "```bash\n",
    "pip install -U pytest\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create your first test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets look at the above example of `pytest` based test file.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# -*- coding: utf-8 -*-\n",
    "\"\"\"\n",
    "Created on Fri May 12 04:16:14 2017.\n",
    "\n",
    "@author: johri_m.\n",
    "\"\"\"\n",
    "\n",
    "\n",
    "def increment(x):\n",
    "    \"\"\".\"\"\"\n",
    "    return x + 1\n",
    "\n",
    "\n",
    "def test_will_fail():\n",
    "    \"\"\".\"\"\"\n",
    "    assert increment(3) == 5\n",
    "\n",
    "\n",
    "def test_will_pass():\n",
    "    \"\"\".\"\"\"\n",
    "    assert increment(4) == 5\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above code, `increment` is the function under test. and `test_will_fail` and `test_will_pass` are the test methods.\n",
    "\n",
    "> **<center>Note</center>**\n",
    "> <hr>\n",
    "> Both the test methods name start with `test`. So all test functions should have name start with `test`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- https://wiki.python.org/moin/PythonTestingToolsTaxonomy"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0b1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
