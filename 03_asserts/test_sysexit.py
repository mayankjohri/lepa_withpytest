# content of test_sysexit.py
import pytest


def f():
    raise SystemExit("Exiting")


def test_exception_pass():
    with pytest.raises(SystemExit):
        f()


def test_exception_message():
    with pytest.raises(SystemExit) as e:
        f()
    assert "Exiting" in e, f"Error Message: {e}"


def test_exception_failure():
    f()
